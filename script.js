// Описати своїми словами навіщо потрібні функції у програмуванні.
// Они позволяют повторно использовать код и уменшить количество дублированого кода.
// Описати своїми словами, навіщо у функцію передавати аргумент.
// Аргумент - значени что передаються в функцию для использования всередини функции. Аргументы могут быть использованы для передачи данных в функцию  или настройки поведение
// Що таке оператор return та як він працює всередині функції?
// Ключевое слово которое указует на то что функция возвращает значения. Когда функция доходит до return она возвращает значение и завершает работу.


function calculate(num1, num2, operator) {
    let result;
    switch (operator) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            console.log("Invalid operator");
    }
    return result;
}

const firstNumber = prompt("Введіть перше число:");
const secondNumber = prompt("Введіть друге число:");
const operator = prompt("Введіть математичну операцію (+, -, *, /):");

const result = calculate(+firstNumber, +secondNumber, operator);
console.log(result);
